﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoreWinFormsApp
{
    public partial class Form1 : Form
    {
        private readonly ILogger _logger;
        //private readonly IBusinessLayer _business;

        public Form1(ILogger<Form1> logger)
        {
            _logger = logger;
            InitializeComponent();
        }

        private void bntClick_Click(object sender, EventArgs e)
        {
            try
            {
                _logger.LogInformation("Form1 { BusinessLayerEvent} at { dateTime} ", "Started", DateTime.UtcNow);
                MessageBox.Show("Hello .Net Core 3.0 ! This is forms app in .NET Core 3.0");
                _logger.LogInformation("Form1 {BusinessLayerEvent} at {dateTime}", "Ended", DateTime.UtcNow);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
